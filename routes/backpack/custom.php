<?php

// --------------------------
// Custom Backpack Routes
// --------------------------
// This route file is loaded automatically by Backpack\Base.
// Routes you generate using Backpack\Generators will be placed here.

Route::group([
    'prefix'     => config('backpack.base.route_prefix', 'admin'),
    'middleware' => ['web', config('backpack.base.middleware_key', 'admin')],
    'namespace'  => 'App\Http\Controllers\Admin',
], function () { // custom admin routes
    CRUD::resource('salon', 'SalonCrudController');
    CRUD::resource('employer', 'EmployerCrudController');
    CRUD::resource('salons_rating', 'SalonRatingCrudController');
    CRUD::resource('employers_rating', 'EmployerRatingCrudController');
    CRUD::resource('setting', 'SettingCrudController');
    CRUD::resource('swiper', 'SwiperCrudController');
    CRUD::resource('video', 'VideoCrudController');
    CRUD::resource('service', 'ServiceCrudController');
}); // this should be the absolute last line of this file