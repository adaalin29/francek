<?php
 
namespace App\Http\Controllers\Api;
 
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Setting;
use App\Models\Video;
use App\Models\Swiper;
 
class SettingsController extends Controller
{
 
    public function index()
    {
      $settings = Setting::all()->first();
      if(!$settings) {
        return [
          'response' => false
        ];      
      }
    // Check if video is selected for homescreen type       
      if($settings['type_home'] == 0) {
        $video = Video::all()->first();
        return [
          'response' => true,
          'type_home' => $settings['type_home'],
          'video' => $video['video']
        ]; 
      }
      
      $swiper = Swiper::all();
      foreach($swiper[0]->images as $object)
      {
          $arrays[] =  $object;
      }
      return [
        'response' => true,
        'type_home' => $settings['type_home'],
        'swiper' => $swiper[0]->images,
        'array_test' => $arrays
      ]; 
           
    }
}