<?php

use Illuminate\Http\Request;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

// Getters(request data from a specified resource)
Route::get('/getSalons', 'SalonsController@index');
 
Route::get('/getEmployers', 'EmployersController@index');

Route::get('/getServices', 'ServicesController@index');

Route::get('/getSettings', 'SettingsController@index');

// Send data to server to create/update a resource
Route::post('/updateSalonsRating','SalonsController@updateRating');
 
Route::post('/updateEmployersRating','EmployersController@updateRating');
 