<?php
 
namespace App\Http\Controllers\Api;
 
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Employer;
use App\Models\EmployerRating;
 
class EmployersController extends Controller
{
 
    public function index()
    {
      $employers = Employer::all();
      if(!$employers) {
        return [
          'response' => false
        ];      
      }
      return [
        'response' => true,
        'employers' => $employers
      ];      
    }
 
    public function updateRating(Request $request)
    {
        $employer = EmployerRating::create($request->all());
        return response()->json($employer, 201);
    }

}