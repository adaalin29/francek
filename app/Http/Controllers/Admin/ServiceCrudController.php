<?php

namespace App\Http\Controllers\Admin;

use Backpack\CRUD\app\Http\Controllers\CrudController;

// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\ServiceRequest as StoreRequest;
use App\Http\Requests\ServiceRequest as UpdateRequest;

/**
 * Class ServiceCrudController
 * @package App\Http\Controllers\Admin
 * @property-read CrudPanel $crud
 */
class ServiceCrudController extends CrudController
{
    public function setup()
    {
        /*
        |--------------------------------------------------------------------------
        | CrudPanel Basic Information
        |--------------------------------------------------------------------------
        */
        $this->crud->setModel('App\Models\Service');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/service');
        $this->crud->setEntityNameStrings('service', 'services');

        /*
        |--------------------------------------------------------------------------
        | CrudPanel Configuration
        |--------------------------------------------------------------------------
        */

        // TODO: remove setFromDb() and manually define Fields and Columns
        // $this->crud->setFromDb();
      
        // Columns shown on page Services
      
        $this->crud->addColumn([
            'name'   => 'image', 
            'label'  => 'Image',
            'type'   => 'image',
            'height' => '100px',
            'prefix' => 'storage/',
        ]);
        $this->crud->addColumn([
            'name'  => 'name', 
            'label' => 'Title',
            'type'  => 'text',
        ]);
      
        $this->crud->addColumn([
          'name'        => 'type',
          'label'       => 'Type',
          'type'        => 'radio',
          'options'     => [
                              0 => "Service1 (no pictures)",
                              1 => "Service2 (with pictures)"
                          ]
        ]);
      
        //Fields for Add or Edit in Services
        
        $this->crud->addField([
          'name' => 'type',
          'type' => 'radio_custom',
          'label' => "Type",
          'options'     => [
                              0 => "Service1 (no pictures)",
                              1 => "Service2 (with pictures)"
                          ]
        ]);
      
        $this->crud->addField([
          'name' => 'name',
          'type' => 'text',
          'label' => "Service name"
        ]);
      
         $this->crud->addField([
            'name'   => 'image',
            'label'  => 'Service Image',
            'type'   => 'image',
            'upload' => true,
            'disk'   => 'public',
        ], 'both');
      
        $this->crud->addField([
            'name'   => 'images',
            'label'  => 'Service Images( upload if service type 2 is selected!)',
            'type'   => 'upload_multiple_custom',
            'upload' => true,
            'disk'   => 'public',
        ], 'both');
      
        $this->crud->addField([
            'name'            => 'contents',
            'label'           => 'Content of every service field',
            'type'            => 'table_custom',
            'entity_singular' => 'content',
            'min'             => 1,
            'columns'         => [
                'name'  => 'Title',
                'service' => 'Services (name and price)',
            ],
        ], 'both');
      
        
        // add asterisk for fields that are required in ServiceRequest
        $this->crud->setRequiredFields(StoreRequest::class, 'create');
        $this->crud->setRequiredFields(UpdateRequest::class, 'edit');
    }

    public function store(StoreRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::storeCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }

    public function update(UpdateRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::updateCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }
}
