<?php

namespace App\Models;

use Image;
use Storage;
use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\CrudTrait;

class Service extends Model
{
    use CrudTrait;

    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    protected $table = 'services';
    // protected $primaryKey = 'id';
    // public $timestamps = false;
    // protected $guarded = ['id'];
    protected $fillable = ['name','image','type','contents','images'];
    // protected $hidden = [];
    // protected $dates = [];
    protected $casts = [
        'images' => 'array'
    ];

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
    // Create mutator for single image attribute
    public function setImageAttribute($value)
    {
        $attribute_name = 'image';
        $disk = 'public';
        $destination_path = 'services';

        if ($value==null) {
            Storage::disk($disk)->delete($this->{$attribute_name});
            $this->attributes[$attribute_name] = null;
        }
        if (starts_with($value, 'data:image')) {
            $image = Image::make($value);
            $filename = md5($value.time()).'.jpg';
            Storage::disk($disk)->put($destination_path.'/'.$filename, $image->stream('jpg'));
            $this->attributes[$attribute_name] = $destination_path.'/'.$filename;
        }
    }
    
   // Create mutator for multiple images attribute 
   public function setImagesAttribute($value)
    {
        $attribute_name = "images";
        $disk = "public";
        $destination_path = "services/services_item";
     
        $this->uploadMultipleFilesToDisk($value, $attribute_name, $disk, $destination_path);
    }
  
   // Delete all images
   public static function boot()
    {
        parent::boot();
        static::deleting(function($obj) {
          
          // Delete image from services directory  
          \Storage::disk('public')->delete($obj->image);
              
          // If there are multiple images, delete it
          if (count((array)$obj->images)) {
              foreach ($obj->images as $file_path) {
                  \Storage::disk('public')->delete($file_path);
              }
          }
        });
    }
  
}
