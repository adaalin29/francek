<?php

namespace App\Http\Controllers\Admin;

use Backpack\CRUD\app\Http\Controllers\CrudController;

// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\SettingRequest as StoreRequest;
use App\Http\Requests\SettingRequest as UpdateRequest;

/**
 * Class SettingCrudController
 * @package App\Http\Controllers\Admin
 * @property-read CrudPanel $crud
 */
class SettingCrudController extends CrudController
{
    public function setup()
    {
        /*
        |--------------------------------------------------------------------------
        | CrudPanel Basic Information
        |--------------------------------------------------------------------------
        */
        $this->crud->setModel('App\Models\Setting');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/setting');
        $this->crud->setEntityNameStrings('setting', 'settings');

        /*
        |--------------------------------------------------------------------------
        | CrudPanel Configuration
        |--------------------------------------------------------------------------
        */

        // TODO: remove setFromDb() and manually define Fields and Columns
        // $this->crud->setFromDb();
      
        // Remove button delete to prevent accidentally deleting setting
        $this->crud->removeButton('delete');
        $this->crud->removeButton('create');
      
        $this->crud->addColumn([
          'name'        => 'setting_name',
          'label'       => 'Name',
          'type'        => 'text'
        ]);
      
        $this->crud->addColumn([
          'name'        => 'type_home',
          'label'       => 'Type',
          'type'        => 'radio',
          'options'     => [
                              0 => "Video",
                              1 => "Swiper"
                          ]
        ]);
      
        $this->crud->addField([
          'name' => 'type_home',
          'type' => 'radio',
          'label' => "Type",
          'options'     => [
                              0 => "Video",
                              1 => "Swiper"
                          ]
        ]);

        // add asterisk for fields that are required in SettingRequest
        $this->crud->setRequiredFields(StoreRequest::class, 'create');
        $this->crud->setRequiredFields(UpdateRequest::class, 'edit');
    }

    public function store(StoreRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::storeCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }

    public function update(UpdateRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::updateCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }
}
