<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;
use Illuminate\Foundation\Http\FormRequest;
use App\Models\Swiper;

class SwiperRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        // only allow updates if the user is logged in
        return backpack_auth()->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'images' => [
                'required',
                function ($attribute, $value, $fail) {
//                   dd($value);
                  $request = request();
                  $imagini = 0;
                  if($swiper_imagini = Swiper::find($request->id)) {
                    $imagini = count($swiper_imagini->images);
                  }
                  if($request->clear_images) {
                      $imagini = $imagini - count($request->clear_images);
                  }
                  if(count($request->images) && !empty($request->images[0])) {
                      $imagini += count($request->images);
                  }
                  if($imagini < 2) {
                    $fail('You need to upload minimum 1 file');
                  }
                },
            ]
        ];
    }

    /**
     * Get the validation attributes that apply to the request.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            //
        ];
    }

    /**
     * Get the validation messages that apply to the request.
     *
     * @return array
     */
    public function messages()
    {
        return [
            //                    
        ];
    }
}
