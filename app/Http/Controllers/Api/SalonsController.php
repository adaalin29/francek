<?php
 
namespace App\Http\Controllers\Api;
 
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Salon;
use App\Models\SalonRating;
 
class SalonsController extends Controller
{
 
    public function index()
    {
      $salons = Salon::all();
      if(!$salons) {
        return [
          'response' => false
        ];      
      }
      return [
        'response' => true,
        'salons' => $salons
      ];      
    }
 
    public function updateRating(Request $request)
    {
        $salon = SalonRating::create($request->all());
        return response()->json($salon, 201);
    }

}