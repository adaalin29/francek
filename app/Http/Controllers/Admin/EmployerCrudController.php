<?php

namespace App\Http\Controllers\Admin;

use Backpack\CRUD\app\Http\Controllers\CrudController;

// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\EmployerRequest as StoreRequest;
use App\Http\Requests\EmployerRequest as UpdateRequest;

/**
 * Class EmployerCrudController
 * @package App\Http\Controllers\Admin
 * @property-read CrudPanel $crud
 */
class EmployerCrudController extends CrudController
{
    public function setup()
    {
        /*
        |--------------------------------------------------------------------------
        | CrudPanel Basic Information
        |--------------------------------------------------------------------------
        */
        $this->crud->setModel('App\Models\Employer');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/employer');
        $this->crud->setEntityNameStrings('employer', 'employers');

        /*
        |--------------------------------------------------------------------------
        | CrudPanel Configuration
        |--------------------------------------------------------------------------
        */

        // TODO: remove setFromDb() and manually define Fields and Columns
        //$this->crud->setFromDb();
      
         $this->crud->addColumn([
            'name'   => 'image', 
            'label'  => 'Image',
            'type'   => 'image',
            'height' => '100px',
            'prefix' => 'storage/',
        ]);
        $this->crud->addColumn([
            'name'  => 'name', 
            'label' => 'Name',
            'type'  => 'text',
        ]);
        
        $this->crud->addColumn([
            'name'  => 'role', 
            'label' => 'Managing position',
            'type'  => 'text',
        ]);
      
        $this->crud->addColumn([
            'name'  => 'average_rate_total', 
            'label' => 'Average Rating',
            'type'  => 'text',
        ]);
          
        $this->crud->addField([
          'name' => 'name',
          'type' => 'text',
          'label' => "Name"
        ]);
        $this->crud->addField([
          'name' => 'role',
          'type' => 'text',
          'label' => "Managing position"
        ]);
        
        $this->crud->addField([
            'name'   => 'image',
            'label'  => 'Employer Image',
            'type'   => 'image',
            'upload' => true,
            'disk'   => 'public',
        ], 'both');
      
        // add asterisk for fields that are required in EmployerRequest
        $this->crud->setRequiredFields(StoreRequest::class, 'create');
        $this->crud->setRequiredFields(UpdateRequest::class, 'edit');
    }

    public function store(StoreRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::storeCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }

    public function update(UpdateRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::updateCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }
}
