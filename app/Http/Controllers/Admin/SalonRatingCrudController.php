<?php

namespace App\Http\Controllers\Admin;

use App\Models\Salon;
use Backpack\CRUD\app\Http\Controllers\CrudController;

// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\SalonsRatingRequest as StoreRequest;
use App\Http\Requests\SalonsRatingRequest as UpdateRequest;

/**
 * Class Salons_ratingCrudController
 * @package App\Http\Controllers\Admin
 * @property-read CrudPanel $crud
 */
class SalonRatingCrudController extends CrudController
{
    public function setup()
    {
        /*
        |--------------------------------------------------------------------------
        | CrudPanel Basic Information
        |--------------------------------------------------------------------------
        */
        $this->crud->setModel('App\Models\SalonRating');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/salons_rating');
        $this->crud->setEntityNameStrings('salon rating', 'salon rating');
        
      
        // Remove Add/Edit/Delete buttons from view to prevent editing informations        
        $this->crud->removeAllButtons();
        /*
        |--------------------------------------------------------------------------
        | CrudPanel Configuration
        |--------------------------------------------------------------------------
        */
        $this->crud->addColumn([
            'name'  => 'id_salon', 
            'label' => 'Salon id',
            'type'  => 'text',
        ]);
        $this->crud->addColumn([
            'name'  => 'max_rate_total_rate',
            'label' => 'Rating / Max-rate / Total rating',
            'type'  => 'string',
        ]);
      
        $this->crud->addColumn([
            'name'  => 'created_at', 
            'label' => 'Date',
            'type'  => 'text',
        ]);
       $this->crud->addColumn([
            'label'     => 'Salon',
            'type'      => 'select',
            'name'      => 'id_salon',
            'entity'    => 'salon',
            'attribute' => 'name',
            'model'     => 'App\Models\Salon',
        ]);
      

    }

    public function store(StoreRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::storeCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }

    public function update(UpdateRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::updateCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }
}
