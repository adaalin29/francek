<!-- This file is used to store sidebar items, starting with Backpack\Base 0.9.0 -->
<li><a href="{{ backpack_url('dashboard') }}"><i class="fa fa-dashboard"></i> <span>{{ trans('backpack::base.dashboard') }}</span></a></li>
<li class="treeview">
  <a href="#"><i class='fa fa-scissors'></i> <i class="fa fa-angle-left pull-right"></i>
    <span>Salons</span>
  </a>
  <ul class="treeview-menu">
        <li><a href="{{ backpack_url('salon') }}"><i class="fa fa-list"></i><span>Salons list</span></a></li>
        <li><a href="{{ backpack_url('salons_rating') }}"><i class="fa fa-star"></i><span>Salons rating</span></a></li>
    </ul>
</li>
<li class="treeview">
  <a href="#"><i class='fa fa-address-book'></i> <i class="fa fa-angle-left pull-right"></i>
    <span>Employers</span>
  </a>
  <ul class="treeview-menu">
        <li><a href="{{ backpack_url('employer') }}"><i class="fa fa-list-alt"></i><span>Employers list</span></a></li>
        <li><a href="{{ backpack_url('employers_rating') }}"><i class="fa fa-star"></i><span>Employers rating</span></a></li>
    </ul>
</li>
<li class="treeview">
  <a href="#"><i class='fa fa-files-o'></i> <i class="fa fa-angle-left pull-right"></i>
    <span>Homescreen Media</span>
  </a>
  <ul class="treeview-menu">
        <li><a href="{{ backpack_url('swiper') }}"><i class="fa fa-file-image-o"></i><span>Swiper</span></a></li>
        <li><a href="{{ backpack_url('video') }}"><i class="fa fa-video-camera"></i><span>Video</span></a></li>
    </ul>
</li>
<li>
  <a href="{{ backpack_url('service') }}"><i class='fa fa-tags'></i> <i class="fa pull-right"></i><span>Services</span></a>
</li>
<li>
  <a href="{{ backpack_url('setting') }}"><i class='fa fa-cog'></i> <i class="fa pull-right"></i><span>Settings</span></a>
</li>
