<?php

namespace App\Http\Controllers\Admin;

use Backpack\CRUD\app\Http\Controllers\CrudController;

// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\EmployerRatingRequest as StoreRequest;
use App\Http\Requests\EmployerRatingRequest as UpdateRequest;

/**
 * Class RatingEmployerCrudController
 * @package App\Http\Controllers\Admin
 * @property-read CrudPanel $crud
 */
class EmployerRatingCrudController extends CrudController
{
    public function setup()
    {
        /*
        |--------------------------------------------------------------------------
        | CrudPanel Basic Information
        |--------------------------------------------------------------------------
        */
        $this->crud->setModel('App\Models\EmployerRating');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/employers_rating');
        $this->crud->setEntityNameStrings('employer rating', 'employer rating');

      
        // Remove Add/Edit/Delete buttons from view to prevent editing informations        
        $this->crud->removeAllButtons();
        /*
        |--------------------------------------------------------------------------
        | CrudPanel Configuration
        |--------------------------------------------------------------------------
        */
        
        $this->crud->addColumn([
            'name'  => 'id', 
            'label' => '#No',
            'type'  => 'text',
        ]);
        $this->crud->addColumn([
            'label'     => 'Employer',
            'type'      => 'select',
            'name'      => 'id_employer',
            'entity'    => 'getEmployer',
            'attribute' => 'name',
            'model'     => 'App\Models\Employer',
        ]);
        $this->crud->addColumn([
            'name'  => 'max_rate_total_rate',
            'label' => 'Rating / Max-rate / Total rating',
            'type'  => 'string',
        ]);
      
         $this->crud->addColumn([
            'name'  => 'comment',
            'label' => 'Comment',
            'type'  => 'text',
        ]);
      
        $this->crud->addColumn([
            'name'  => 'created_at', 
            'label' => 'Date',
            'type'  => 'text',
        ]);
       

        // TODO: remove setFromDb() and manually define Fields and Columns
        //$this->crud->setFromDb();

        // add asterisk for fields that are required in RatingEmployerRequest
        $this->crud->setRequiredFields(StoreRequest::class, 'create');
        $this->crud->setRequiredFields(UpdateRequest::class, 'edit');
    }

    public function store(StoreRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::storeCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }

    public function update(UpdateRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::updateCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }
}
