<?php
 
namespace App\Http\Controllers\Api;
 
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Service;
 
class ServicesController extends Controller
{
 
    public function index()
    {
      $services = Service::all();
      if(!$services) {
        return [
          'response' => false
        ];      
      }
      foreach($services as $object) {
          $object->contents =  json_decode($object->contents,true);
        
          $content = $object->contents;
            
//           foreach($content as &$content_modified) {
            
//             $content_modified['service'] = str_replace([
//                 "<table>", 
//                 "</table>", 
//                 "<tbody>", 
//                 "</tbody>", 
//             ], "", $content_modified['service']);
//           }
//         $object->contents = $content;
      }
      return [
        'response' => true,
        'services' => $services
      ];      
    }
}