<?php

namespace App\Models;

use Image;
use Storage;
use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\CrudTrait;

class Swiper extends Model
{
    use CrudTrait;

    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    protected $table = 'swipers';
    protected $primaryKey = 'id';
    // public $timestamps = false;
    // protected $guarded = ['id'];
    protected $fillable = ['images'];
    protected $casts = [
        'images' => 'array'
    ];
    // protected $hidden = [];
    // protected $dates = [];

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
  
     // Create mutator for single image attribute
     public function setImagesAttribute($value)
    {
       $attribute_name = "images";
        $disk = "public";
        $destination_path = "swipers";
     
        $this->uploadMultipleFilesToDisk($value, $attribute_name, $disk, $destination_path);
    }
  
   // Delete all images
   public static function boot()
    {
        parent::boot();
        static::deleting(function($obj) {
              
          // If there are multiple images, delete it
          if (count((array)$obj->images)) {
              foreach ($obj->images as $file_path) {
                  \Storage::disk('public')->delete($file_path);
              }
          }
        });
    }
}
