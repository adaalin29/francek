<?php

namespace App\Http\Controllers\Admin;

use Backpack\CRUD\app\Http\Controllers\CrudController;

// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\SalonRequest as StoreRequest;
use App\Http\Requests\SalonRequest as UpdateRequest;

/**
 * Class SalonCrudController
 * @package App\Http\Controllers\Admin
 * @property-read CrudPanel $crud
 */
class SalonCrudController extends CrudController
{
    public function setup()
    {
        /*
        |--------------------------------------------------------------------------
        | CrudPanel Basic Information
        |--------------------------------------------------------------------------
        */
        $this->crud->setModel('App\Models\Salon');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/salon');
        $this->crud->setEntityNameStrings('salon', 'salons');

        /*
        |--------------------------------------------------------------------------
        | CrudPanel Configuration
        |--------------------------------------------------------------------------
        */

        // TODO: remove setFromDb() and manually define Fields and Columns
        //$this->crud->setFromDb();
        // Add 2 fields name and image uploader   
      
        // Columns shown on page Salons
      
        $this->crud->addColumn([
            'name'   => 'image', 
            'label'  => 'Image',
            'type'   => 'image',
            'height' => '100px',
            'prefix' => 'storage/',
        ]);
        $this->crud->addColumn([
            'name'  => 'name', 
            'label' => 'Name',
            'type'  => 'text',
        ]);
        $this->crud->addColumn([
            'name'  => 'average_rate_total', 
            'label' => 'Average Rating',
            'type'  => 'string',
        ]);
      
        //Fields for Add or Edit in Salons
      
        $this->crud->addField([
          'name' => 'name',
          'type' => 'text',
          'label' => "Salon name"
        ]);
      
        $this->crud->addField([
            'name'   => 'image',
            'label'  => 'Salon Image',
            'type'   => 'image',
            'upload' => true,
            'disk'   => 'public',
        ], 'both');

        // add asterisk for fields that are required in SalonRequest
        $this->crud->setRequiredFields(StoreRequest::class, 'create');
        $this->crud->setRequiredFields(UpdateRequest::class, 'edit');
    }

    public function store(StoreRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::storeCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }

    public function update(UpdateRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::updateCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }
}
